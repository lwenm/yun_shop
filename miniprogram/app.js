import * as Comm from './util/comm.js';
import * as Api from './util/api.js';
import * as Db from './util/db.js';
import Validate from './util/validate.js';
import lUtil from './util/loginUtil.js';
import * as Config from './util/config';

let userInfo = Db.get("userInfo") || {}
let isLogin = JSON.stringify(userInfo) != "{}"

App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: 'music-7gt2a70c03ae61ce',
        traceUser: true,
      })
    }

    this.globalData = {
      userInfo: userInfo,
      isLogin: isLogin
    }


    //判断登录过期
    if (!lUtil.checkExpiresTime()) {
      setTimeout(() => {
        this.globalData.userInfo = {}
        this.globalData.isLogin = false
        this.$db.del("userInfo")
      }, 1500)
      wx.showModal({
        title: '提示',
        content: '登录超时，请重新授权登录！',
        showCancel: false,
        confirmText: '确定',
        success: () => {
          this.$comm.reLaunch("/pages/user/user")
        }
      });
    }

  },

  $comm: Comm,
  $api: Api,
  $db: Db,
  $validate: Validate,
  $lUtil: lUtil,
  $config:Config
})